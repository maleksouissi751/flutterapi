<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = ['code', 'name', 'address', 'phone1', 'phone2', 'price', 'date', 'state', 'comment', 'governorate', 'is_payed', 'seller_id'];
}
