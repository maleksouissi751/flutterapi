<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryMan extends Model
{
    protected $table='delivery_men';
    protected $fillable=['fname','lname','phone'];
}
