<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class OrderController extends Controller
{
    public function create(Request $request)
    {
        //
        $orders = new Order();
        $orders->code = $request->input('code');
        $orders->name = $request->input('name');
        $orders->address = $request->input('address');
        $orders->phone1 = $request->input('phone1');
        $orders->phone2 = $request->input('phone2');
        $orders->date = $request->input('date');
        $orders->state = $request->input('state');
        $orders->comment = $request->input('comment');
        $orders->governorate = $request->input('governorate');
        //$orders->is_payed = $request->input('is_payed');
        $orders->seller_id = $request->input('seller_id');
        $orders->save();
        return response()->json($orders);
    }

    public function show(Order $order)
    {
        //
        $orders = Order::all();
        return response()->json($orders);
    }

    public function showId($id)
    {
        //
        $orders = Order::find($id);
        return response()->json($orders);
    }

    public function update(Request $request, $id)
    {
        //
        $orders = Order::find($id);
        $orders->code = $request->input('code');
        $orders->name = $request->input('name');
        $orders->address = $request->input('address');
        $orders->phone1 = $request->input('phone1');
        $orders->phone2 = $request->input('phone2');
        $orders->date = $request->input('date');
        $orders->state = $request->input('state');
        $orders->comment = $request->input('comment');
        $orders->governorate = $request->input('governorate');
        $orders->is_payed = $request->input('is_payed');
        $orders->seller_id = $request->input('seller_id');
        $orders->save();
        return response()->json($orders);
    }

    public function destroy(Request $request , $id)
    {
        //
        $orders = Order::find($id);
        $orders->delete();
        return response()->json($orders);
    }
}
