<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DeliveryMan;


class DeliveryManController extends Controller
{
    public function create(Request $request){
        $delivery_men = new DeliveryMan();

        $delivery_men->fname= $request->input('fname');
        $delivery_men->lname= $request->input('lname');
        $delivery_men->phone= $request->input('phone');

        $delivery_men->save();
        return response()->json($delivery_men);
    }

    public function show(){
        $delivery_men= DeliveryMan::all();
        return response()->json($delivery_men);

    }

    public function showbyId($id){
        $delivery_men = DeliveryMan::find($id);
        return response()->json($delivery_men);

    }

    public function updatebyId(Request $request,$id){
        $delivery_men = DeliveryMan::find($id);
        
        $delivery_men->fname= $request->input('fname');
        $delivery_men->lname= $request->input('lname');
        $delivery_men->phone= $request->input('phone');

        $delivery_men->save();

        return response()->json($delivery_men);
    }


    public function deletebyId(Request $request, $id){
        $delivery_men = DeliveryMan::find($id);

        $delivery_men->delete();
        return response()->json($delivery_men);

    }
}
