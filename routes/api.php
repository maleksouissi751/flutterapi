<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('login', 'PassportController@login');
Route::post('register', 'PassportController@register');

Route::post('/DeliveryMan','DeliveryManController@create');
Route::get('/delivery_men','DeliveryManController@show');
Route::get('/delivery_men/{id}','DeliveryManController@showbyId');
Route::put('/DeliveryManupdate/{id}','DeliveryManController@updatebyId');
Route::delete('/DeliveryMandelete/{id}','DeliveryManController@deletebyId');

Route::post('/order', 'OrderController@create');
Route::get('/orders', 'OrderController@show');
Route::get('/orders/{id}', 'OrderController@showId');
Route::put('/orderupdate/{id}' , 'OrderController@update');
Route::delete('/orderdelete/{id}' , 'OrderController@destroy');


Route::middleware('auth:api')->group(function () {
    Route::get('user', 'PassportController@details');
 
    Route::resource('products', 'ProductController');
});